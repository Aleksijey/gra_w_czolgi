#pragma once
#include <list>
#include <map>
#include "Data.h"
#include <SFML\Network.hpp>
#include "player.h"
class GameInfo
{
public:
	std::map<std::string, Data> players;
	std::list<Bullet> bullets;

	friend sf::Packet& operator << (sf::Packet& output, GameInfo& input)
	{
		for (std::map<std::string, Data>::iterator i = input.players.begin(); i != input.players.end(); ++i)
			output << i->second.player << i->second.x << i->second.y << i->second.degTank << i->second.degGun << i->second.shoot << i->second.turretX << i->second.turretY;
		output << (std::string)"##%##";
		return output;
	}

	friend sf::Packet& operator >> (sf::Packet& output, GameInfo& input)
	{
		while (output != NULL)
		{
			Data tmp;
			output >> tmp.player;
			if (tmp.player == "##%##")
				break;
			output >> tmp.x >> tmp.y >> tmp.degTank >> tmp.degGun >> tmp.shoot >> tmp.turretX >> tmp.turretY;
			input.players.insert(std::pair<std::string, Data>(tmp.player, tmp));
		}
		while (output != NULL)
		{
			Bullet tmp(0,0);
			output >> tmp.x >> tmp.y;
			if (tmp.x == -9999 && tmp.y == 9999)
				break;
			input.bullets.push_front(tmp);
		}
		return output;
	}

	GameInfo();
	~GameInfo();
};
