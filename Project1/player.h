#pragma once
#include "tank.h"
using namespace std;
using namespace sf;


class player : public tank
{
public:

	player(int a, int b, string name) :tank(a, b, name) {
		if (!playerTexture.loadFromFile("textures/tank.png") || !turretTexture.loadFromFile("textures/gun.png"))
		{
			cout << "Fail at loading texture" << endl;
			system("pause");
		}// wczytanie tekstury z pliku
		 //---------------------------inicjalizowanie korpusu ------------------------------------
		playerBody.setTexture(&playerTexture); // wsadzenie ca�ej tekstury do pude�ka palyerBody
		playerBody.setTexture(&playerTexture);
		//----------------------------inicjalizowanie wie�yczki ---------------------------------
		turretBody.setTexture(&turretTexture);

	}
	~player();
	float turretToMouse();
	void update();
	Vector2f checkFront(int, int);
	Vector2f checkBack(int, int);
};

class enemy : public tank
{
public:


	enemy(int a, int b, string name) :tank(a ,b, name) {
		if (!playerTexture.loadFromFile("textures/enemy_tank.png") || !turretTexture.loadFromFile("textures/enemy_gun.png"))
		{
			std::cout << "Fail at loading texture" << std::endl;
			system("pause");
		}// wczytanie tekstury z pliku
		playerBody.setTexture(&playerTexture); // wsadzenie ca�ej tekstury do pude�ka palyerBody									   
		turretBody.setTexture(&turretTexture); //----------------------------inicjalizowanie wie�yczki ---------------------------------
	}
	~enemy();
};


class background
{
public:
	RectangleShape imageShape;
	Texture image;
	
	background(int borderX,int  borderY)
	{
		this->imageShape.setPosition(0, 0);
		this->imageShape.setSize(Vector2f(borderX, borderY));
		if (!image.loadFromFile("textures/background.jpg"))
		{
			std::cout << "fail at loading background texture" << std::endl;
		}
		imageShape.setTexture(&image);
	};
	~background()
	{
	};

};


class Bullet
{
public:
	CircleShape body;
	int x;
	int y;
	Bullet(int x, int y) : x(x), y(y)
	{
		body.setRadius(5);
		body.setFillColor(sf::Color::White);
		//body.setPosition(x, y);
	};
	~Bullet()
	{};
};

