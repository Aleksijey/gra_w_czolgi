
#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <iostream>
#include <math.h>
using namespace sf;
class tank{
public:
	std::string name;
	int degree;
	float rotation;
	double degree2, px, py;
	RectangleShape turretBody, playerBody;
	Rect <float> size, turretSize;
	Texture playerTexture, turretTexture;
	Vector2f moveJump, turretVector;
	Vector2i mousePosition;

	void turretDirection(float);
	void MoveBackward(Vector2f);
	void moveForward(Vector2f);

	tank(int, int, std::string);
	~tank();
};

