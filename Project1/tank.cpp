#include "tank.h"



tank::tank(int x, int y, std::string name) // konstruktor 
{
	playerBody.setPosition(x, y); // pozycja korpusu
	turretBody.setPosition(x, y); // pozycja wie�yczki
	playerBody.setSize(Vector2f(60,70));  // wielko�� prostok�ta z korpusem
	turretBody.setSize(Vector2f(35, 70)); // wielko�� prostok�ta z wie�yczk�
	playerBody.setOrigin(30, 35); // �rodek korpusu
	turretBody.setOrigin(17, 48); //  �rodek wie�yczki	
	this->name = name;
}
void tank::moveForward(Vector2f moveJump)
{
	this->playerBody.move(moveJump);
	this->turretBody.move(moveJump);
}
void tank::MoveBackward(Vector2f moveJump)
{
	this->playerBody.move(-moveJump);
	this->turretBody.move(-moveJump);
}

void tank::turretDirection(float degr)
{
	turretBody.setRotation(degr);
}

tank::~tank()
{
}
