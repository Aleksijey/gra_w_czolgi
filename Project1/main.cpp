#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <iostream>
#include <map>
#include "player.h"
#include <stdio.h>
#include "data.h"
#include "GameInfo.h"
using namespace std;
using namespace sf;
// sta�e szeroko�ci okna (mo�na zmieni�)-|
const int borderX = 1500;           //---|
const int borderY = 800;				//---|
// --------------------------------------|

int main()
{
	//------------------------------------------- po��czenie 1 ---------------------------------------------------
	TcpSocket socket;
	IpAddress ip = "127.0.0.1";
	unsigned int port = 53000;
	Data data;
	GameInfo info;
	try
	{
		if (socket.connect(ip, port) != Socket::Done)
		{
			throw (std::string)"Nie mo�na nasluchiwac na porcie: " + std::to_string(port);
		}
	}
	catch (std::string e)
	{
		cerr << e << endl;
	}
	//---------------------------------------------------------------------------------------------------------




	sf::RenderWindow window(sf::VideoMode(borderX, borderY), "Tanks wars"); // Tworzenie okna
	window.setFramerateLimit(30);											// co ile ma si� od�wierza�
	player player1(100,100,"andrzej");	//Player1
	background b1(borderX, borderY);
	map<string, enemy> enemies;			// Lista z przeciwnikami
	int recoil = 0;
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		} // zamkni�cie przez krzy�yk
//---------------------------------------------- poruszanie si� ----------------------------------------------//

			if (Keyboard::isKeyPressed(Keyboard::Key::W))
			{
				player1.update();
				player1.moveForward(player1.checkFront(borderX, borderY));
			}
			if (Keyboard::isKeyPressed(Keyboard::Key::S))
			{
				player1.update();
				player1.MoveBackward(player1.checkBack(borderX, borderY));
			}
			if (Keyboard::isKeyPressed(Keyboard::Key::D))
			{
				player1.update();
				player1.playerBody.setRotation(((int)player1.playerBody.getRotation()+5) % 360);
			}
			if (Keyboard::isKeyPressed(Keyboard::Key::A))
			{
				player1.update(); 
				player1.playerBody.setRotation(((int)player1.playerBody.getRotation() + 355) % 360);
			}

//---------------------------------------------- obr�t wie�yczki + strza� -----------------------------------------//
				
				player1.mousePosition = Mouse::getPosition(window);
				player1.turretDirection(player1.turretToMouse());
//---------------------------------------------- ******************* ----------------------------------------------//








				info.players.clear();
//--------------------------------------------- po��czenie 2 ---------------------------------------------------//
				Packet packet;
				data.degGun = player1.turretToMouse();		 // wysy�anie k�ta obrotu wiezy
				data.x = player1.playerBody.getPosition().x; // wysy�anie wsp�rz�dnych
				data.y = player1.playerBody.getPosition().y; // wysy�anie wsp�rz�dnych
				data.degTank = (int)player1.playerBody.getRotation(); // wysy�anie k�ta obrotu nadwozia
				data.player = player1.name;					 //wysy�anie nazwy
				data.shoot = false;							 //czy w�a�nie strzeli�
				data.turretX = player1.turretBody.getPosition().x;
				data.turretY = player1.turretBody.getPosition().y;
				info.bullets.clear();
//------------------------------------------------------------- STRZA� -----------------------------------------//
				if (Mouse::isButtonPressed(Mouse::Left) && recoil >= 60)
				{
					cout << "strzaaaaaaaaaaaaaaaaaaa�!" << endl;
					data.shoot = true;
					recoil = 0;
				
				}
				if(recoil<60)
					recoil++;

//--------------------------------------------------------------------------------------------------------------//

				packet << data;
				try
				{
					if (socket.send(packet) != sf::Socket::Done)
					{
						throw (string)"Blad przy wysy�aniu danych!";
					}
				}
				catch (string e)
				{
					cerr << e << endl;
				}
				try
				{
					if (socket.receive(packet) != sf::Socket::Done)
					{
						throw (string)"Blad przy odbiorze danych!";
					}
				}
				catch (string e)
				{
					cerr << e << endl;
				}
				
				 // paczka danych z serwera do wy�wietlenia przeciwnika 

				window.clear();
				window.draw(b1.imageShape);
				//
				info.players.clear();
				packet >> info;
				
				for (std::map<std::string, Data>::iterator i = info.players.begin(); i != info.players.end(); ++i)
				{
					
					
				
					if (i->first != data.player)
					{
						enemy enemyTmp(i->second.x, i->second.y, i->first);
						//cout << i->second.player << endl;
						enemyTmp.playerBody.setRotation(i->second.degTank);
						enemyTmp.turretBody.setRotation(i->second.degGun);
						//enemies.insert(pair<string, enemy>(i->first,enemyTmp));
						
						window.draw(enemyTmp.playerBody);
						window.draw(enemyTmp.turretBody);
					}
				}
//------------------------------------------------ latanie pocisk�w -----------------------------------------------//
				for (std::list<Bullet>::iterator i = info.bullets.begin(); i != info.bullets.end(); ++i) {
					cout << "x =  " <<i->x<<" y = " << i->y<< endl;
					Bullet bulletTmp(i->x, i->y);
			
					bulletTmp.body.setPosition(i->x, i->y);
					window.draw(bulletTmp.body);
				}
//-----------------------------------------------------------------------------------------------------------------//
				//cout << "bool "<<data.shoot << endl;





//-------------------------------------------------- umiejscawianie przeciwnika -----------------------------------//


//-----------------------------------------------------------------------------------------------------------------//
		
		
		
		window.draw(player1.playerBody);
		window.draw(player1.turretBody);
	
		window.display();

	
	}
	return 0;
}

