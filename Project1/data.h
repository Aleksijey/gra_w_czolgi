#pragma once
#include <iostream>
#include <SFML\Network.hpp>
class Data
{
public:
	std::string player;
	int x;
	int y;
	int degTank;
	int turretX, turretY;
	double degGun;
	bool shoot;

	friend sf::Packet& operator >> (sf::Packet& input, Data& output)
	{
		input >> output.player >> output.x >> output.y >> output.degTank >> output.degGun >> output.shoot >> output.turretX >> output.turretY;
		return input;
	}

	friend sf::Packet& operator << (sf::Packet& input, Data& output)
	{
		input << output.player << output.x << output.y << output.degTank << output.degGun << output.shoot << output.turretX << output.turretY;
		return input;
	}


	Data();
	~Data();
};
